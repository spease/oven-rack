#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Provides a number of utility functions used throughout the rest of the OvenRack project.
"""

# Standard Library
import copy
import importlib
import os
import sys
from operator import add

REGISTERED_MODULES = {}


def log_header(log_handler, header):
    """ Given a logger, a log level, and a header, generate a debug header. """
    log_handler('')
    log_handler('')
    log_handler('#' * os.get_terminal_size().columns)
    header = header.split('\n')
    for line in header:
        log_handler('# ' + line)
    log_handler('#')
    log_handler('')


def get_list(obj, default=None):
    """ Many things in our recipes can be either a value or a list of values. get_list allows us to
    always treat these values as a list. We can also assume a default if there is no value there."""
    if default is None:
        default = []
    if not obj:
        return default
    if isinstance(obj, list):
        return obj
    return [obj, ]


def merge_yaml(original, new):
    """ Do a non-destructive deep merge of two YAML configs. The second config will be preferred,
    and any lists will be concatenated, unless the list is suffixed with a bang (!).
    In that case, the list of the first is replaced with the list of the second."""
    merged = copy.deepcopy(original)
    for orig_obj in new:
        obj = orig_obj.rstrip('!')
        if obj in merged:
            if isinstance(original[obj], dict):
                merged[obj] = merge_yaml(original[obj], new[orig_obj])
            elif isinstance(original[obj], list):
                if orig_obj[-1] == '!':
                    merged[obj] = get_list(new[orig_obj])
                else:
                    merged[obj] = get_list(original[obj]) + get_list(new[orig_obj])
            else:
                merged[obj] = new[orig_obj]
        else:
            merged[obj] = new[orig_obj]
    return merged


def progress_bar(block_num, block_size, total_size):
    """ Give a progress bar for the completion rate of an image download. """
    console_width = os.get_terminal_size().columns
    width = console_width - 10
    percent = (block_num * block_size) / total_size
    progress = '#' * int(percent * width) + ' ' * (width - int(percent * width))
    percent_done = str(int(percent * 100)).rjust(3) + "%"
    print(f"\r[ {progress} {percent_done} ]", end='')


def print_table(rows, titles=None, print_with=print):
    """ Given an array of arrays, print table. """
    if titles is None:
        titles = []

    screen_width = os.get_terminal_size().columns
    columns = []
    max_width = []  # max width of each column according to it's longest cell

    had_titles = bool(titles)

    # find out the row with the most columns, we'll need to pad our the rows and titles to match.
    max_columns = max(list(map(len, rows)) + [len(titles)])
    if diff := (max_columns - len(titles)) > 0:
        titles.extend([''] * diff)

    columns = transpose_and_fill(rows, max_columns)

    # easily calculate the largest item in each column.
    for column in columns:
        max_width.append(max(map(len, column)))
    # add the widths of each column, and one space for each column.
    total_min_width = len(columns) + sum(max_width)

    # calculate how much extra space we have to use between each column
    additional_widths = (screen_width - total_min_width) // (len(columns) - 1)
    additional_widths = [additional_widths] * (len(columns) - 1) + [0]
    # add the additional_width to each column, except the last
    max_width = list(map(add, additional_widths, max_width))

    if had_titles and rows:  # only print titles if we have titls and things to print
        builder = ''.join([title.ljust(max_width[i]) + ' ' for i, title in enumerate(titles)])
        print_with(builder)
        print_with('-' * screen_width)

    for row in rows:
        builder = ''.join([item.ljust(max_width[i]) + ' ' for i, item in enumerate(row)])
        print_with(builder)


def transpose_and_fill(rows, max_columns):
    """ Transpose the array to make column widths easier to calculate. We're also
    going to fill in any columns that don't have data as well.
    This modifies rows to be filled out as well. """
    columns = []
    y = 0
    while len(rows) > 0 and y < len(rows):
        row = rows[y]
        diff = max_columns - len(row)
        if diff > 0:
            row.extend([''] * diff)
        for x, item in enumerate(row):
            if len(columns) < x + 1:
                columns.append([])
                columns[-1].extend([''] * y)
            columns[x].append(item)
        y += 1
    return columns


def load_modules(extra_modules):
    """ Given paths where extra modules might be found, we load any modules that have been installed into
    OvenRack (relative) and any paths that were defined in the config (extra_modules). """
    files = []
    relative = [os.path.join(os.path.realpath(os.getcwd()), 'modules')]
    extra_modules = [os.path.expanduser(module) for module in extra_modules]
    module_paths = [*relative, *extra_modules]
    for path in module_paths:
        if os.path.exists(path):
            files += [(path, file) for file in os.listdir(path)]
    for path, file in files:
        if file.endswith('.py'):
            spec = importlib.util.spec_from_file_location(file, os.path.join(path, file))
            module = importlib.util.module_from_spec(spec)
            module.loaded_from = os.path.join(path, file)
            spec.loader.exec_module(module)

    return REGISTERED_MODULES


def service(module_name):
    """ Decorator that allows us to register service modules. """
    def inner_decorator(cls):
        cls.path = sys.modules[cls.__module__].__file__
        REGISTERED_MODULES[module_name] = cls
        return cls
    return inner_decorator
