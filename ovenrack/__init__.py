"""Top-level module for OvenRack. Sets version."""

# OvenRack Libraries
from ovenrack.version import VERSION

__version__ = VERSION
__version_info__ = tuple(int(i) for i in __version__.split(".") if i.isdigit())
