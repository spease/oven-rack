#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Provides all utility functions and classes for mounting disk images with their appropriate filesystems
and mount points.
"""

# Standard Library
import contextlib
import lzma
import os
import pathlib
import shutil
import struct
import subprocess
import sys
import tempfile
import time
import urllib.request
import zipfile

# OvenRack Libraries
from ovenrack.utils import progress_bar


class MountedImage:
    """
    Mount an image in it's entirety, and then return a handler where it can be modified by an
    Instruction. Provides path() to access things within the mount.
    """
    def __init__(self, config):
        self.config = config
        self.guest_fs = {}
        self.image = None
        self.partitions = {}
        self.final_image_path = None

    def __enter__(self):
        self.image = self._get_image()  # get image from internet or cache, then make a working copy
        try:
            self.partitions, boot_mount_point = self._mount()  # actually mount everything
        except Exception as e:
            self.__exit__(*sys.exc_info())
            raise e
        root_device = self._examine_bootloader(boot_mount_point)  # look at the boot partition and find /
        self._get_fstab(root_device)  # look in /etc/fstab to build our mount point library.

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._umount()

    def _get_image(self):
        # Get the image mentioned in the config either from cache or the internet, then make a working
        # copy for us to work with.
        image_url = self.config.data['_image-url']
        image_name = pathlib.Path(image_url).stem
        local_images_folder = self._get_host_path('images')

        if not os.path.exists(local_images_folder):
            os.mkdir(local_images_folder)

        image_path = self._get_host_path('images', image_name + ".img")
        if not os.path.exists(image_path):
            try:
                print(f"Downloading {image_name}...")
                filename, headers = urllib.request.urlretrieve(image_url, reporthook=progress_bar)
                if headers.get('Content-Type', '') == 'application/zip':
                    print(f"\nUnzipping {image_name}...")
                    with zipfile.ZipFile(filename) as f:
                        f.extractall(local_images_folder)
                elif headers.get('Content-Type', '') == 'application/x-xz':
                    print(f"\nUncompressing {image_name}...")
                    with open(os.path.join(local_images_folder, f"{image_name}.img"), 'wb') as outf:
                        with contextlib.closing(lzma.LZMAFile(filename)) as inf:
                            while True:
                                data = inf.read(1024 * 1024)
                                if not data:
                                    break
                                outf.write(data)
            finally:
                os.remove(filename)
        else:
            print(f"Using images/{image_name}.img")

        # use our CLI flag if we have one, otherwise default to matching the name of the YAML file.
        self.final_image_path = self.config.args.output or f"{self.config.args.config.rsplit('.', 1)[0]}.img"
        self.final_image_path = os.path.join(os.getcwd(), self.final_image_path)
        print('Creating image...')
        try:
            os.remove(self.final_image_path)
        except FileNotFoundError:
            pass
        shutil.copy(image_path, self.final_image_path)
        return self.final_image_path

    def _mount(self):
        # Given what we know about the image's disk layout and offsets from fdisk, actually mount
        # all the relevant partitions. We also try to figure out which partition is the boot partition
        # (probably the first one), so we can search it for /etc/fstab and actually figure out what the
        # image's filesystem is supposed to look like and set up self.guest_fs.
        if sys.platform.startswith('linux') or sys.platform.startswith('freebsd'):
            boot_mount_point = None
            fdisk, boot_uuid = self._fdisk()

            for device, metadata in fdisk.items():
                offset = 512 * metadata['start_sector']
                sizelimit = 512 * metadata['num_sectors']
                try:
                    mount_point = tempfile.mkdtemp(prefix='ovenrack_')
                    options = f'loop,rw,offset={offset},sizelimit={sizelimit}'
                    subprocess.run(['/bin/mount', '-o', options, self.image, mount_point], check=True)
                except subprocess.CalledProcessError as e:
                    os.rmdir(mount_point)
                    self._umount()
                    raise SystemExit('Failed to mount partition {device}.') from e
                self.partitions[device] = mount_point
                if device == boot_uuid:
                    boot_mount_point = mount_point
        elif sys.platform.startswith('darwin'):
            pass  # TBI
        elif sys.platform.startswith('win32'):
            pass  # TBI

        return self.partitions, boot_mount_point

    def _umount(self):
        # Try to unmount everything we've mounted, partitions-first, then clean up our temp folders.
        for device in sorted(self.partitions)[::-1]:  # longest first, so we get partitions first
            mount_point = self.partitions[device]
            successfully_unmounted = False
            while not successfully_unmounted:
                try:
                    if sys.platform.startswith('linux') or sys.platform.startswith('freebsd'):
                        subprocess.run(['/bin/umount', mount_point], check=True)
                    elif sys.platform.startswith('darwin'):
                        pass  # TBI
                    elif sys.platform.startswith('win32'):
                        pass  # TBI
                    os.rmdir(mount_point)
                    successfully_unmounted = True
                except subprocess.CalledProcessError:
                    print(f"{mount_point} is busy. Retrying...")
                    time.sleep(5)

    def _fdisk(self):
        # Read the MBR to figure out what our partitions look like, so we can mount them later.
        fdisk = {}
        with open(self.image, 'rb') as f:
            mbr = f.read(512)
        if not mbr[-2:] == b'U\xaa':  # 0x55 0xAA
            raise OSError("Invalid or corrupted primary image.")
        uuid = mbr[440:444][::-1].hex()
        part_entries = mbr[446:-2]  # 64 bytes, 4x 16byte partition entries.
        part_entries = [part_entries[start:start + 16] for start in range(0, 64, 16)]
        for i, part_entry in enumerate(part_entries, start=1):
            partuuid = f"PARTUUID={uuid}-{i:0>2}"
            names = ['status', 'start_CHS', 'type', 'end_CHS', 'start_sector', 'num_sectors']
            partition_info = struct.unpack('<b3sb3sii', part_entry)
            partition_info = dict(zip(names, partition_info))
            if partition_info['num_sectors'] > 0:
                fdisk[partuuid] = partition_info
            if i == 1:
                boot_uuid = partuuid
                if partition_info['type'] == 0xEE:
                    raise OSError("GPT partitioning found. Only MBR is currently supported.")
        return fdisk, boot_uuid

    def _examine_bootloader(self, boot_mount_point):
        # Given the location of the mount point for boot, find some reference to where root is in the
        # kernel arguments via the bootloader. With an rpi, that's cmdline.txt.
        self.guest_fs = {'/': boot_mount_point}  # temp value until we find fstab

        if os.path.exists(self._get_guest_path("/cmdline.txt")):
            with open(self._get_guest_path("/cmdline.txt"), encoding='utf-8') as f:
                cmdline = f.read().split(' ')
                cmd = {arg.split('=', 1)[0]: arg.split('=', 1)[-1] for arg in cmdline}
                root = cmd['root']
        else:
            raise OSError("Unknown bootloader. Cannot resolve rootfs.")
        self.guest_fs['/'] = self.partitions[root]
        return root

    def _get_fstab(self, device):
        # Now that we know which partition is the root partition, mount it, and read /etc/fstab so we know
        # what other filesystems we have.
        if device in self.partitions:
            with open(self._get_guest_path("/etc/fstab"), encoding='utf-8') as f:
                for line in f.readlines():
                    if line.strip()[0] == "#" or line.strip() == "":
                        continue
                    # device, mount_point, fs, options, dump, pass
                    names = ['device', 'mount_point', 'fs', 'options', 'dump', 'fspass']
                    fstab = line.split()
                    fstab = dict(zip(names, fstab))
                    if fstab['mount_point'] != 'none' and fstab['device'] in self.partitions:
                        self.guest_fs[fstab['mount_point']] = self.partitions[fstab['device']]
        else:
            raise OSError(f"Kernel rootfs {device} not found in partition table.")

    def path(self, full_path):
        """ Get the host path to a file in the image. """
        if full_path.startswith('~'):
            raise OSError("Destination path {full_path} cannot include a ~ character- user is ambiguous.")
        return self._get_guest_path(full_path.lstrip(os.path.sep))

    def _get_host_path(self, *path):
        # Get a file on the host computer relative to ovenrack itself.
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), *path)

    def _get_guest_path(self, *path):
        # Internal function to get a file in the host image.
        given_path = os.path.join(*path)
        try:
            best_match = [x for x in sorted(self.guest_fs, key=len)[::-1] if given_path.startswith(x)][0]
        except IndexError as exc:
            raise OSError(f"Cannot resolve invalid path {given_path} within the guest recipe.") from exc
        given_path = given_path.lstrip(os.path.sep)
        return os.path.join(self.guest_fs[best_match], given_path)
