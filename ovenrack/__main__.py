#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module allowing for ``python -m ovenrack ...``.
"""
# OvenRack Libraries
from ovenrack.cli import main

if __name__ == "__main__":
    raise SystemExit(main())
