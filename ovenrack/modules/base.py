#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Provides the Base Service Module that all other modules inherit from. Modules consume YAML configs and
produce Instructions that are then applied inside the image. Several basic modules are included as well.
"""

# Standard Library
import base64
import ctypes
import logging
import os
import re

# OvenRack Libraries
from ovenrack.instructions import CopyFile, CreateDir, CreateFile, FilePermission, ModifyFile, SymLinkFile
from ovenrack.utils import log_header, service

dll_crypt = ctypes.CDLL('libcrypt.so').crypt
dll_crypt.restype = ctypes.c_char_p

LOG = logging.getLogger(__name__)


class BaseModule():
    required_fields = []
    default_fields = []
    default_modules = []
    module_name = 'BaseModule'

    def __init__(self, recipe, parent=None):
        self.recipe = recipe
        self.registered_modules = {module.module_name: module for module in self.default_modules}
        self.image = None
        if not parent:
            self.root = self
            self.installs = []
            self.first_start = []
            self.instructions = []
        else:
            self.root = parent.root
        for field_name in self.default_fields:
            setattr(self, f'field_{field_name}', self.handle_default_field)

    def handle_default_field(self, recipe, field):
        setattr(self, field, recipe)
        return []

    def create_instruction(self):
        return []

    def add_program(self, program):
        self.root.installs.append(program)

    def generate_first_start(self):
        invocation = self.root.recipe['_package-manager']
        contents = "#!/bin/sh\nif [ -f /usr/local/etc/first_start ]; then"
        contents += "\n\texit 0\nelse\n\t/bin/touch /usr/local/etc/first_start\nfi\n"
        contents += invocation.format(**{'programs': ' '.join(self.root.installs)}) + '\n'
        contents += '\n'.join(self.first_start)
        return [
            ModifyFile('/etc/rc.local', '^exit 0$', '/usr/local/bin/first_start.sh\nexit 0'),
            CreateFile('/usr/local/bin/first_start.sh', contents, FilePermission(mode=0o744)),
        ]

    def handle_modules(self):
        errors = []
        mod_fields = {func: getattr(self, func) for func in dir(self) if func.startswith('field_')}
        missing_fields = list(set(self.required_fields) - set(mod_fields.keys()))
        for missing_field in missing_fields:
            errors.append(f"CRITICAL: {self.__class__.__name__} is missing required field {missing_field}.")
        if missing_fields:
            return errors
        for key in self.recipe:
            if key in self.registered_modules:
                sub = self.registered_modules[key](self.recipe[key], parent=self)
                # register any discovered submodules for sub here.
                errors += sub.handle_modules()
            elif f"field_{key}" in mod_fields:
                errors += mod_fields[f"field_{key}"](self.recipe[key], field=key)
            else:
                if key.startswith('_'):
                    continue
                errors.append((f"{self.__class__.__name__} could find no registered modules for {key}. "
                               f"Check your syntax or make sure you've installed any custom modules into the "
                               f"appropriate locations."))
            # if errors:
                # break
        if not errors:
            self.root.instructions += self.create_instruction()
        return errors

    def generate_instructions(self):
        self.instructions = list(self.instructions) + self.generate_first_start()
        log_header(LOG.debug, 'Generated Instructions')
        for instruction in self.instructions:
            LOG.debug(instruction)
        LOG.debug('')
        log_header(LOG.debug, 'Startup Script')
        LOG.debug(self.instructions[-1].contents)
        LOG.debug('')

    def execute_instructions(self, image):
        self.image = image
        for instruction in self.instructions:
            instruction.execute(self, image)

    class NotAModuleException(Exception):
        pass


class Recipe(BaseModule):
    def __init__(self, config, parent=None):
        self.config = config
        super().__init__(config.data, parent)
        self.registered_modules = config.modules

    def path(self, full_path):
        if full_path.startswith('~'):
            raise OSError("Destination path {full_path} cannot include a ~ character- user is ambiguous.")
        return self.image.path(full_path)


class ListModule(BaseModule):
    def __init__(self, recipe, parent=None):
        self.list = []
        super().__init__(recipe, parent)

    def handle_modules(self):
        for item in self.recipe:
            self.list.append(item)
        self.root.instructions += self.create_instruction()
        return []


@service('install.locale')
class LocaleModule(BaseModule):
    default_modules = []
    default_fields = ['kb', 'locale', 'timezone', 'wifi']
    module_name = 'locale'

    def create_instruction(self):
        ins = []
        ins.append(ModifyFile('/etc/locale.gen', rf'# {re.escape(self.locale)}', f'{self.locale}'))
        ins.append(ModifyFile('/etc/default/keyboard', '^XKBLAYOUT=".*"$', f'XKBLAYOUT="{self.kb}"'))
        ins.append(ModifyFile('/etc/wpa_supplicant/wpa_supplicant.conf',
                              '^country=.*$', f'country={self.wifi}'))
        ins.append(SymLinkFile('/etc/localtime', f'/usr/share/zoneinfo/{self.timezone}'))
        return ins


@service('install.users')
class UsersModule(BaseModule):
    default_modules = []
    module_name = 'users'

    def handle_modules(self, *args, **kwargs):
        uid = 1000
        errors = []
        for username in self.recipe:
            if username.startswith('_'):
                continue
            if isinstance(self.recipe[username], str):
                self.recipe[username] = {'pass': self.recipe[username]}
            user = UserModule(username, self.recipe[username], uid, parent=self)
            uid += 1
            errors += user.handle_modules()
        return errors


class UserModule(BaseModule):
    required_fields = []
    default_modules = []
    default_fields = ['uid', 'gid', 'shell', 'name']

    def __init__(self, username, recipe, uid, *args, **kwargs):
        super().__init__(recipe, *args, **kwargs)
        self.module_name = username
        self.username = username
        self.password = "*"
        self.name = username
        self.ssh = False
        self.shell = "/bin/sh"
        self.uid = uid
        self.gid = 1000

    def crypt(self, password):
        salt = base64.b64encode(os.urandom(16), altchars=b'./').rstrip(b'=')
        return dll_crypt(password.encode(), b"$6$" + salt).decode()

    def field_pass(self, recipe, *args, **kwargs):
        if recipe == "LOCKED":
            self.password = f"*LOCKED*{self.crypt(recipe)}"
        elif recipe == "NOPASS":
            self.password = "*"
        else:
            self.password = f"{self.crypt(recipe)}"
        return []

    def field_ssh(self, recipe, *args, **kwargs):
        if os.path.exists(recipe):
            return [f"{self.__class__.__name__}.ssh does not exist."]
        if os.access(recipe, os.R_OK):
            return [f"{self.__class__.__name__}.ssh cannot be read."]
        self.ssh = recipe
        return []

    def create_instruction(self):
        ins = []
        passwd = self.root.recipe['install']['users']['_passwd']
        shadow = self.root.recipe['install']['users']['_shadow']
        if self.username in self.root.recipe['install']['users']['_default_users']:
            ins.append(ModifyFile(passwd, rf'^{self.username}:(.+):(.+):(.+):(.+):(.+):(.+)$',
                                          (rf'{self.username}:x:{self.uid}:{self.gid}:'
                                           rf'{self.name},,,:\4:{self.shell}')))
            ins.append(ModifyFile(shadow, rf'^{self.username}:(.+):(.+):(.+):(.+):(.+):(.+)$',
                                          (rf'{self.username}:{self.password}:{self.uid}:{self.gid}:'
                                           rf'{self.name},,,:\4:{self.shell}')))
        else:
            ins.append(CreateDir(f"/home/{self.username}", FilePermission(self.uid, self.gid, 0o775)))
            ins.append(ModifyFile(passwd, r'\Z', (rf'\n{self.username}:x:{self.uid}:{self.gid}:'
                                                  rf'{self.name},,,:/home/{self.username}:{self.shell}')))
            ins.append(ModifyFile(shadow, r'\Z', rf'\n{self.username}:{self.password}:17766:0:99999:7:::'))
            if self.shell != "/bin/sh":
                self.add_program(os.path.basename(self.shell))
        if self.ssh:
            ins.append(CreateDir(f"/home/{self.username}/.ssh", FilePermission(self.uid, self.gid, 0o700)))
            fname = os.path.basename(self.ssh)
            ins.append(CopyFile(self.ssh, f"/home/{self.username}/.ssh/{fname}",
                                FilePermission(self.uid, self.gid, 0o600)))
        return ins


@service('install.network.dns')
class DNSModule(BaseModule):
    nameserver = ['8.8.8.8', '8.8.4.4']
    default_fields = ['domain', 'nameserver']
    module_name = 'dns'

    def create_instruction(self):
        return [ModifyFile('/etc/dhcpcd.conf',
                           r'\Z', (rf'\nstatic domain_name_servers={" ".join(self.nameserver)}'
                                   rf'\nstatic domain_search={self.domain}')), ]


@service('install.network.wifi')
class WifiModule(BaseModule):
    module_name = 'wifi'
    default_fields = ['psk', 'ssid']

    def create_instruction(self):
        f = '/etc/wpa_supplicant/wpa_supplicant.conf'
        if self.psk and self.ssid:
            return [ModifyFile(f, r'\Z', rf'\nnetwork={{\n\tssid="{self.ssid}"\n\tpsk="{self.psk}"\n}}"'), ]
        return []


@service('install.network')
class NetworkModule(BaseModule):
    default_modules = [DNSModule, WifiModule]
    module_name = 'network'
    default_fields = ['hostname', 'ntp']

    def create_instruction(self):
        ins = []
        ins.append(ModifyFile('/etc/hostname', r'.*', self.hostname))
        ins.append(ModifyFile('/etc/hosts', r'^127\.0\.1\.1.*$', f'127.0.1.1	{self.hostname}'))
        ins.append(ModifyFile('/etc/systemd/timesyncd.conf',
                              r'\Z', rf'FallbackNTP={" ".join(self.ntp)}'))
        return ins


@service('install.bootup')
class BootupModule(BaseModule):
    module_name = 'bootup'
    default_fields = ['netwait', 'splashscreen']

    def create_instruction(self):
        ins = []
        if not self.splashscreen:
            ins.append(ModifyFile('/boot/cmdline.txt', r' ?splash', ''))
        else:
            ins.append(CopyFile(self.splashscreen, '/usr/share/plymouth/themes/pix/splash.png'))
        if self.netwait:
            ins.append(CreateFile('/etc/systemd/system/dhcpcd.service.d/wait.conf',
                                  """[Service]\nExecStart=\nExecStart=/usr/lib/dhcpcd5/dhcpcd -q -w"""))
        return []


@service('install')
class InstallModule(BaseModule):
    default_modules = [UsersModule, NetworkModule, BootupModule, LocaleModule]
    module_name = 'install'


@service('services.apt')
class AptModule(ListModule):
    default_modules = []
    module_name = 'apt'

    def create_instruction(self):
        for item in self.list:
            self.root.add_program(item)
        return []


@service('services')
class ServicesModule(BaseModule):
    default_modules = [AptModule, ]
    module_name = 'services'


@service('_image-url')
class ImageURLModule(BaseModule):
    default_modules = []
    module_name = 'image-url'

    def handle_modules(self):
        # stub module. this doesn't actually inherantly do anything... yet.
        return []


@service('_arch')
class ArchModule(BaseModule):
    default_modules = []
    module_name = '-arch'

    def handle_modules(self):
        # stub module. this doesn't actually inherantly do anything... yet.
        return []


@service('_package-manager')
class PackageManagerModule(BaseModule):
    default_modules = []
    module_name = '_package-manager'

    def handle_modules(self):
        # stub module. this doesn't actually inherantly do anything... yet.
        return []


@service('post-install')
class PostInstallModule(BaseModule):
    default_modules = []
    module_name = 'post-install'
    shell_commands = []
    files = {}

    def field_run(self, recipe, *args, **kwargs):
        for action in recipe:
            self.shell_commands.append(action)
        return []

    def field_files(self, recipe, *args, **kwargs):
        for file_dict in recipe:
            for key in file_dict:
                self.files[os.path.abspath(key)] = file_dict[key]
        return []

    def create_instruction(self):
        ins = []
        for action in self.shell_commands:
            self.root.first_start.append(action)
        for local_file, dest_file in self.files.items():
            ins.append(CopyFile(local_file, dest_file, FilePermission()))
        return ins
