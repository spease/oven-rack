#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Ovenrack is a method of repeatably creating disk images from a YAML configuration. This is the primary CLI
interface to do so, and does all argument parsing as well as kicking off all relevant actions as a result.
This program produces a disk image such as a .img from a .yaml file, which can then be applied to a
USB drive or similar using dd.
"""

# Standard Library
import argparse
import logging
import os
import sys

# OvenRack Libraries
from ovenrack.config import Config
from ovenrack.disk import MountedImage
from ovenrack.modules.base import Recipe
from ovenrack.version import VERSION

LOG = logging.getLogger('ovenrack')
LOG.setLevel(logging.WARNING)
LOG_SH = logging.StreamHandler()
LOG.addHandler(LOG_SH)


def prechecks(args):
    """ Runs pre-run checks that we have permissions and the expected environment. """
    # Check if the user is root.
    if os.geteuid() != 0 and not args.dry_run:
        raise SystemExit("This program requires root to run.")

    # Check that we're on a supported platform
    if sys.platform.startswith('linux') or sys.platform.startswith('freebsd'):
        return
    if sys.platform.startswith('win32'):
        raise SystemExit('Windows support has not yet been implemented.')
    if sys.platform.startswith('darwin'):
        raise SystemExit('MacOS support has not yet been implemented.')
    raise SystemExit('Unsupported operating system.')


def main():
    """ Entry Point of the CLI interface. """
    argparser = argparse.ArgumentParser(description="Generate repeatable disk images from YAML files.",
                                        usage="ovenrack [-hdVv] config")
    argparser.add_argument('config', help='The recipe file to use')
    argparser.add_argument('-V', '--version', action='version', version=f'%(prog)s {VERSION}',
                           help="Show program's version number and exit.")
    argparser.add_argument('-v', '--verbose', action='store_true', help='Show more log information')
    argparser.add_argument('-d', '--dry-run', action='store_true', help='Do not actually modify any files')
    argparser.add_argument('-o', '--output', help='Name of the output image. Default matches the YAML file.')
    argparser.add_argument('--debug', action='store_true', help=argparse.SUPPRESS)
    args = argparser.parse_args()

    if args.verbose:
        LOG.setLevel(logging.INFO)
    if args.debug:
        LOG.setLevel(logging.DEBUG)
        LOG.debug('Debug mode enabled.')

    prechecks(args)

    if args.config:
        cfg = Config(args)
        recipe = Recipe(cfg)

        if errors_list := recipe.handle_modules():
            for error in errors_list:
                LOG.error(error)
            # SystemExit(-1) commented out- we know we have missing modules for now.

        recipe.generate_instructions()

        if args.dry_run:
            raise SystemExit(0)

        with MountedImage(cfg) as image:
            input("DEBUG: Press enter to execute instructions. ")
            # recipe.execute_instructions(image)
            print('Instructions executed!')
            input("DEBUG: Press enter to unmount and quit. ")
            print("DEBUG: Unmounted.")
            print(f"Your image is located at {image.final_image_path}")


if __name__ == '__main__':
    main()
