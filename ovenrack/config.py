#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Provides functions to load, inspect, transform, and access the YAML config(s) the user provides.
"""

# Standard Library
import logging
import os
from pprint import pformat

# OvenRack Libraries
from ovenrack import utils

# External Dependencies
import yaml

LOG = logging.getLogger(__name__)


class Config():
    """
    The class whose instantiation holds the config and all associated metadata for it.
    """
    def __init__(self, args):
        self.env = {}
        self.args = args
        self.original_path = os.path.dirname(self.args.config)
        self.full_path = self.get_filename(self.args.config)
        self.errors = []

        try:
            with open('.env', 'r', encoding='utf-8') as f:
                for line in f:
                    key, value = line.strip().split('=')
                    self.env[key] = value
        except FileNotFoundError:
            pass
        with open(self.full_path, 'r', encoding='utf-8') as f:
            self.data = self.preprocess(yaml.load(f, yaml.SafeLoader))

        # This is in utilities because it returns the REGISTERED_MODULES dictionary, which is populated
        # via that @service decorator when they're loaded by utils.load_modules.
        self.modules = utils.load_modules(self.data.pop('load', []))

        utils.log_header(LOG.debug, 'Parsed Configuration')
        LOG.debug(pformat(self.data))

        utils.log_header(LOG.debug, 'Loaded Modules')
        pformatted_modules = [(name, module.path) for name, module in sorted(self.modules.items())]
        utils.print_table(pformatted_modules, titles=['Module', 'Source'], print_with=LOG.debug)

    def __repr__(self):
        # Provide a readable representation of Config.
        return self.args.config

    def dumps(self):
        """ Dump the internal state of the config to string. """
        return yaml.safe_dump(self.data, indent=4)

    def get_filename(self, path):
        """ Given a YML file, determine the full path the file. First check the local directory,
        then the local directory modified by the original path to the config, then the defaults directory.
        """
        default = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'os_configs', path)
        relative = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', self.original_path, path)
        if os.path.isfile(path):
            return path
        if os.path.isfile(relative):
            return relative
        if os.path.isfile(default):
            return default
        raise SystemExit(f"The recipe {path} cannot be found.")

    def preprocess(self, data):
        """ Remove and execute any pre-image building configurations, like including other YAML files. """
        ret = {}
        if 'setenv' in data:
            for pair in data['setenv']:
                for key in pair:
                    self.env[key] = pair[key]
            for key, value in self.env.items():
                if not os.environ.get(key, ''):
                    os.environ[key] = str(value)
            del data['setenv']
        if 'image' in data:
            image_default = f"{data['image']}.yaml"
            ret = self._include(ret, image_default)
            del data['image']
        if 'include' in data:
            include_list = utils.get_list(data['include'])
            del data['include']
            for filename in include_list:
                ret = self._include(ret, filename)
        ret = utils.merge_yaml(ret, data)
        return ret

    def _include(self, original_config, filename):
        # Given the original config and another config, merge the two with the new config taking
        # LESSER priority (i.e., the original config is going to take precedence).
        full_path = self.get_filename(filename)
        with open(full_path, 'r', encoding='utf-8') as f:
            new = self.preprocess(yaml.load(f, yaml.SafeLoader))
            original_config = utils.merge_yaml(original_config, new)
        return original_config
