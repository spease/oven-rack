#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Provides Instructions. Each Service Module that ingests it's section of config emits a set of Instructions
that are then applied to the image, one after another, to accomplish the configuration desired.
"""

# Standard Library
import os
import re
from shutil import copy


class FilePermission():  # pylint: disable=too-few-public-methods
    """ Holds the permission set for a file."""
    def __init__(self, uid=0, gid=0, mode=0o600):
        self.uid = uid
        self.gid = gid
        self.mode = mode


def mkdirs(module, path, perms):
    """ Helper function for Instructions to create directories with the appropriate permissions."""
    path = path.rstrip(os.path.sep)
    path_parts = path.split(os.path.sep)
    path = path_parts.pop(0)
    if not path:
        path = os.path.sep
    while os.path.exists(module.path(path)) and path_parts:
        path = os.path.join(path, path_parts.pop(0))
    while len(path_parts) > 0:
        os.mkdir(module.path(path), mode=perms.mode)
        os.chown(module.path(path), perms.uid, perms.gid)
        path = os.path.join(path, path_parts.pop(0))
    try:
        os.mkdir(module.path(path), mode=perms.mode)
        os.chown(module.path(path), perms.uid, perms.gid)
    except FileExistsError:
        pass


class ModifyFile:
    """ Takes a file in the image and applies a `sed -i -e {change}` for each change. """
    def __init__(self, fpath, pattern, replace):
        self.fpath = os.path.expanduser(fpath)
        self.pattern = pattern
        self.replace = replace

    def execute(self, module, image):
        """ Applies the modification to the file within the image."""
        with open(module.path(self.fpath), "r+", encoding='utf-8') as f:
            contents = f.read()
            contents = re.sub(self.pattern, self.replace, contents, flags=re.M | re.S)
            f.seek(0)
            f.truncate()
            f.write(contents)

    def __repr__(self):
        # Provides debug representation of the object.
        return f"[ModifyFile '{self.fpath}' 's/{repr(self.pattern)}/{repr(self.replace)}/gm']"


class CreateFile:
    """ Given a path, creates a file with the contents given. """
    def __init__(self, recipe_fpath, contents, perms):
        self.fpath = os.path.expanduser(recipe_fpath)
        self.contents = contents
        self.perms = perms

    def execute(self, module, image):
        """ Creates the file within the image."""
        mkdirs(module, os.path.split(self.fpath)[0], self.perms)
        with open(module.path(self.fpath), 'x') as f:  # pylint: disable=unspecified-encoding
            f.write(self.contents)
        os.chmod(module.path(self.fpath), self.perms.mode)
        os.chown(module.path(self.fpath), self.perms.uid, self.perms.gid)

    def __repr__(self):
        # Provides debug representation of the object.
        return f"[CreateFile '{self.fpath}']"


class CreateDir:
    """ Given a path, creates a dir with the permissions given. """
    def __init__(self, recipe_fpath, perms):
        self.fpath = os.path.expanduser(recipe_fpath)
        self.perms = perms

    def execute(self, module, image):
        """ Creates the directory within the image."""
        mkdirs(module, self.fpath, self.perms)

    def __repr__(self):
        # Provides debug representation of the object.
        return f"[CreateDir '{self.fpath}']"


class CopyFile:
    """ Given a local file, copy it into the image. """
    def __init__(self, host_fpath, recipe_fpath, perms):
        self.host_fpath = os.path.expanduser(host_fpath)
        self.recipe_fpath = os.path.expanduser(recipe_fpath)
        self.perms = perms

    def execute(self, module, image):
        """Copy the file within the image to another spot inside the image."""
        mkdirs(module, os.path.split(self.recipe_fpath)[0], self.perms)
        copy(self.host_fpath, module.path(self.recipe_fpath))
        os.chown(module.path(self.recipe_fpath), self.perms.uid, self.perms.gid)
        os.chmod(module.path(self.recipe_fpath), self.perms.mode)

    def __repr__(self):
        # Provides debug representation of the object.
        return f"[CopyFile '{self.host_fpath}' -> '{self.recipe_fpath}']"


class SymLinkFile:
    """ Given a file, symlink it to another file in the image. """
    def __init__(self, symlink_fpath, target_fpath):
        self.symlink_fpath = os.path.expanduser(symlink_fpath)
        self.target_fpath = os.path.expanduser(target_fpath)

    def execute(self, module, image):
        """ Apply the symlink on the files inside the image."""
        try:
            os.remove(module.path(self.target_fpath))
        except FileNotFoundError:
            pass
        os.symlink(module.path(self.symlink_fpath), module.path(self.target_fpath))

    def __repr__(self):
        # Provides debug representation of the object.
        return f"[SymLinkFile '{self.symlink_fpath}' -> '{self.target_fpath}']"


class ExecuteShell:
    """ Execute some shell action in the image. Can only be used on devices that support os.system,
    and should only be used as a last resort. Creating a custom module is typically a better idea."""
    def __init__(self, shstr):
        self.shstr = shstr

    def execute(self, module, image):
        """ Create a child process that we chroot into the image, the execute the shell actions. """
        pid = os.fork()
        if pid == 0:
            # Child process
            os.chroot(module.rootfs)
            os.system(self.shstr)
            os._exit(0)  # We do this to avoid accidentally triggering pioven.py's finally on L63 twice.
        else:
            os.wait()

    def __repr__(self):
        # Provides debug representation of the object.
        return f"[ExecuteShell '{self.shstr}']"
