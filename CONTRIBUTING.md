To contribute code, please fork the repo, make a branch for your feature, and then do a pull request against your branch.

Thank you for contributing!