# OvenRack

Using a configuration file, generate an up-to-date OS Image for quick deployment. 

```
usage: ovenrack [-hdVveo] config

Generate repeatable disk images from YAML files.

positional arguments:
  config                The recipe file to use

options:
  -h, --help            show this help message and exit
  -V, --version         Show program's version number and exit.
  -v, --verbose         Show more log information
  -d, --dry-run         Do not actually modify any files
  -e, --env FILE        Location of file to pull environment variables from
  -o, --output FILE     Name of the output image. Default matches the YAML file.
```

Configurations are YAML files. Demonstrations can be found in examples/.
